'use strict';

var autoprefixerList = [
  'Chrome >= 45',
  'Firefox ESR',
  'Edge >= 12',
  'Explorer >= 10',
  'iOS >= 9',
  'Safari >= 9',
  'Android >= 4.4',
  'Opera >= 30'
];

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/',
		favicons: "build/img/favicons/",
		sprites: "build/img/sprites/",
	},
	src: {
		html: 'src/*.html',
		js: 'src/js/*.js',
		vendorJs: [
			'src/libs/jquery/dist/jquery.min.js',
			'src/libs/bootstrap/dist/js/bootstrap.min.js',
			'src/libs/swiper/dist/js/swiper.js',
			'src/libs/datepicker.js',
			// 'src/js/wow.js',
	],
		style: 'src/style/dranikss.scss',
		vendorCss: [
		'src/libs/bootstrap/dist/css/bootstrap.min.css',
			'src/libs/swiper/dist/css/swiper.css',
			'src/libs/datepicker.min.css',
	],
		img: [
		'src/img/**/*.*',
		'!src/img/favicons/favicon.{jpg,jpeg,png,gif}',
		'!src/img/icons/svg/*'
	],
		fonts: 'src/fonts/**/*.*',
		favicons: 'src/img/favicons/favicon.{jpg,jpeg,png,gif}',
		sprites: 'src/img/icons/svg/*.svg',
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		css: 'src/style/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'srs/fonts/**/*.*',
	},
	deploy: [
		'build/**',
		//'build/.htaccess',
	],
	clean: './build/*'
};

var config = {
	server: {
		baseDir: './build'
	},
	notify: false
};

var gulp = require('gulp'), 
	webserver = require('browser-sync'), 
	plumber = require('gulp-plumber'),
	rigger = require('gulp-rigger'),
	sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cleanCSS = require('gulp-clean-css'),
	uglify = require('gulp-uglify'), 
	cache = require('gulp-cache'),
	imagemin = require('gulp-imagemin'),
	jpegrecompress = require('imagemin-jpeg-recompress'),
	pngquant = require('imagemin-pngquant'),
	rimraf = require('gulp-rimraf'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	replace = require('gulp-replace'),
	favicons = require('gulp-favicons'),
	gulpif = require('gulp-if'),
	svgSprite = require('gulp-svg-sprites'),
	gutil = require('gulp-util'),
	ftp = require('vinyl-ftp'),
    cheerio = require('gulp-cheerio'),
	yargs = require('yargs');

const argv = yargs.argv;
const production = !!argv.production;

gulp.task('deploy', function() {
	var conn = ftp.create({
		host:      '',
		user:      '',
		password:  '',
		parallel:  10,
		log: gutil.log
	});

	return gulp.src(path.deploy, {buffer: false})
	.pipe(conn.dest('/www/'));
});

gulp.task('webserver', function () {
	webserver(config);
});

gulp.task('html:build', function () {
	return gulp.src(path.src.html)
		.pipe(plumber())
		.pipe(rigger())
		.pipe(gulpif(production, replace("dranikss.css", "dranikss.min.css")))
		.pipe(gulpif(production, replace("dranikss.js", "dranikss.min.js")))
		.pipe(gulp.dest(path.build.html))
		.pipe(webserver.reload({
			stream: true
		}));
});

gulp.task('css:build', function () {
	return gulp.src(path.src.style)
		.pipe(plumber())
		.pipe(gulpif(!production, sourcemaps.init()))
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: autoprefixerList
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulpif(production, cleanCSS()))
		.pipe(gulpif(!production, sourcemaps.write('./')))
		.pipe(gulp.dest(path.build.css))
		.pipe(webserver.reload({
			stream: true
		}));
});

gulp.task('js:build', function () {
	return gulp.src(path.src.js)
		.pipe(plumber())
		.pipe(rigger())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulpif(!production, sourcemaps.init())) 
		.pipe(gulpif(production, uglify())) 
		.pipe(gulpif(!production, sourcemaps.write('./'))) 
		.pipe(gulp.dest(path.build.js)) 
		.pipe(webserver.reload({
			stream: true
		})); 
});

gulp.task('vendorJs:build', function () {
	return gulp.src(path.src.vendorJs)
		.pipe(plumber())
		.pipe(uglify())
		.pipe(concat('vendor.min.js'))
		.pipe(gulp.dest(path.build.js));
});

gulp.task('vendorCss:build', function () {
	return gulp.src(path.src.vendorCss)
		.pipe(plumber())
		.pipe(cleanCSS())
		.pipe(concat('vendor.min.css'))
		.pipe(gulp.dest(path.build.css));
});

gulp.task('fonts:build', function () {
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts));
});

gulp.task('image:build', function () {
	return gulp.src(path.src.img) 
		.pipe(cache(imagemin([
      imagemin.gifsicle({
				interlaced: true
			}),
      jpegrecompress({
				progressive: true,
				max: 90,
				min: 90
			}),
      pngquant(),
      imagemin.svgo({
				plugins: [{
					removeViewBox: false
				}]
			})
    ])))
		.pipe(gulp.dest(path.build.img)); // выгрузка готовых файлов
});

gulp.task('favs:build', function () {
	return gulp.src(path.src.favicons)
		.pipe(plumber())
		.pipe(favicons({
			icons: {
				appleIcon: true,
				favicons: true,
				online: false,
				appleStartup: false,
				android: false,
				firefox: false,
				yandex: false,
				windows: false,
				coast: false
			}
		}))
		.pipe(gulp.dest(path.build.favicons))

});

gulp.task('sprites:build', function () {
	return gulp.src(path.src.sprites)
		.pipe(plumber())
        .pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[style]').removeAttr('style');
                $('[stroke]').removeAttr('stroke');
			},
			parserOptions: { xmlMode: true }
		}))
		.pipe(svgSprite({
				mode: "symbols",
				preview: false,
				selector: "%f",
				svg: {
					symbols: 'sprite.svg'
				}
			}

		))
		.pipe(gulp.dest(path.build.sprites))
});

gulp.task('clean:build', function () {
	return gulp.src(path.clean, {
			read: false
		})
		.pipe(rimraf());
});

gulp.task('cache:clear', function () {
	cache.clearAll();
});

gulp.task('build',
	gulp.series('clean:build',
		gulp.parallel(
			'html:build',
			'css:build',
			'js:build',
			'vendorCss:build',
			'vendorJs:build',
			'fonts:build',
			'image:build',
			'favs:build',
			'sprites:build'
		)
	)
);

gulp.task('watch', function () {
	gulp.watch(path.watch.html, gulp.series('html:build'));
	gulp.watch(path.watch.css, gulp.series('css:build'));
	gulp.watch(path.watch.js, gulp.series('js:build'));
	gulp.watch(path.watch.img, gulp.series('image:build'));
	gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
});

// задача по умолчанию
gulp.task('default', gulp.series(
	'build',
	gulp.parallel('webserver', 'watch')
));

gulp.task('ftp', gulp.series(
	'build',
	'deploy'
));