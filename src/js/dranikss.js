$(function() {
    // console.group( "Start: dev" );

    new WOW({offset: 68}).init();

    // прелоадер
    $('body').animate({'opacity':1}, 1500, 'linear');
    $('.preloader').animate({'opacity':0, 'dispay':'none'}, 1000, 'linear', function () {
        $(this).hide();
    });


    function createFeaturesSlider(){
        $('.apartments.apartments-list .container').not('.slick-initialized').slick();
    }
    function disabledFeaturesSlider(){
        $('.apartments.apartments-list .container').slick('unslick');
    }

    if($(window).width() < 780) {
        createFeaturesSlider();
    }

    $(window).resize(function(e) {
        if(e.target.innerWidth < 780){
            createFeaturesSlider();
        } else{
            disabledFeaturesSlider();
        }
    });


    // атоматическое добавление фонового изображения по data атрибутам
    $("div[data-src],a[data-src]").each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data("src").split(",");
        var result = "";
        if (src.length >= 2) {
            for (var i = 0; i < src.length; i++) {
                if (i == src.length - 1) {

                } else {
                    result += " url(" + src[i] + "),";
                }
            }
        } else
            result = " url(" + src[0] + ")";
        elem.css("background-image", result);
    }

    // клик по бургеру
    $('body').on('click','.header--burger', function (e) {
        e.preventDefault();
        $(this).toggleClass('header--burger-open');
        $('.header--menu').toggleClass('header--menu-open');
    });

    // клик по языку
    $('body').on('click','.header--lang', function (e) {
        e.preventDefault();
        $(this).toggleClass('header--lang_active');
        $('.header--lang-list').toggleClass('header--lang-list_active');
    });

    $('body').on('click', function (e) {
        if ($(e.target).closest(".header--lang").length === 0) {
            $('.header--lang-list').removeClass('header--lang-list_active');
            // $(".header--lang-list").hide();
        }
    });



    // фокус на форме и элементах инпут
    $('body').on('change','.form-control', function (e) {
        e.preventDefault();
        if($(this).val() == ''){
            $(this).removeClass('focused');
        } else{
            $(this).addClass('focused');
        }
    });

    $('body').on('click', '.bookline--drop', function (e) {
        e.preventDefault();
        $(this).parent().parent().find('input').click();

    })



    // добавление апартамента в бронировании
    $('body').on('click','.ap--check', function (e) {
        e.preventDefault();
        if(!$(this).hasClass('ap--check-active')){

            $(this).addClass('ap--check-active');
            $('.modal-book--chooise').append('<div class="ap--chooise" data-title="'+$(this).data('title')+'">'+$(this).data('title')+' <a href="#" class="ap--chooise-del"><svg xmlns="http://www.w3.org/2000/svg" width="45" height="45" viewBox="0 0 45 45">\n' +
                '    <g fill="none" fill-rule="evenodd" stroke="#B37931" stroke-linejoin="round">\n' +
                '        <path d="M35.728 35.304L10.272 9.848M10.396 35.528L36.452 9.472"/>\n' +
                '    </g>\n' +
                '</svg></a></div>');
        }
    });

    // удаление апартамента в бронировании
    $('body').on('click','.ap--chooise-del', function (e) {
        e.preventDefault();
        var dataTitle = $(this).parent().data('title');
        $('.ap--check.ap--check-active[data-title="'+dataTitle+'"]').removeClass('ap--check-active');
        $(this).parent().remove();
    });



    // слайдер на главной
    var swiperMain = new Swiper('.swiper-container', {
        loop:true,
        effect:'slide',
        speed:600,
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.m-slider--next',
            prevEl: '.m-slider--prev',
        }
    });


    var datepickerFrom = $('.bookpicker-from').datepicker({
        dateFormat:'dd.mm',
        position:'bottom center',
        autoClose:true,
        minDate:new Date()
    }).data('datepicker');

    var datepickerTo = $('.bookpicker-to').datepicker({
        dateFormat:'dd.mm',
        position:'bottom center',
        autoClose:true,
        minDate:new Date()
    }).data('datepicker');

    var datepickerFromModal = $('.bookpicker-from-modal').datepicker({
        dateFormat:'dd.mm',
        position:'bottom center',
        autoClose:true,
        minDate:new Date()
    }).data('datepicker');

    var datepickerToModal = $('.bookpicker-to-modal').datepicker({
        dateFormat:'dd.mm',
        position:'bottom center',
        autoClose:true,
        minDate:new Date()
    }).data('datepicker');

    // слайдер с апартаментами на главной
    var swiperAp = new Swiper('.apartaments', {
        loop:true,
        effect:'slide',
        speed:700,
        slidesPerView: 3,
        navigation: {
            nextEl: '.m-slider--next',
            prevEl: '.m-slider--prev'
        },
        breakpoints: {
            991: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            1199: {
                slidesPerView: 2,
                spaceBetween: 0
            }
        }
    });

    // плавный скрол по якорю
    $('body').on('click', '.apa--ankor', function (e) {
        e.preventDefault();
        var _href = $(this).attr("href");
        $("html, body").animate({scrollTop: $(_href).offset().top+"px"}, 600);
        return false;
    });


    $('body').on('click', '.apartments--item, .swiper-slide .ap', function(e){
        e.preventDefault()
        $('#apartment').modal('show');
    })

    $('body').on('click', '.btn-book', function(e){
        e.preventDefault()
        $('#book').modal('show');

    });
    // drop
    $('.bookline-black').on('click', '.bookline--drop.book-from', function(e){
        e.preventDefault();
        datepickerFrom.show();
    })
    $('.bookline-black').on('click', '.bookline--drop.book-to', function(e){
        e.preventDefault();
        datepickerTo.show();
    })
    // dropmodal
    $('.modal-book').on('click', '.bookpicker-from-modal', function(e){
        e.preventDefault();
        datepickerFromModal.show();
    })
    $('.modal-book').on('click', '.b-to .bookpicker', function(e){
        e.preventDefault();
        datepickerToModal.show();
    })

    $('.modal-book').on('click', '.book-quest, input[name="book-quest"]', function(e){
        e.preventDefault();
        countInput('.modal-book .b-quest');

    })

    $('.modal-book').on('click', '.book-child, input[name="book-child"]', function(e){
        e.preventDefault();
        countInput('.modal-book .b-child');
    })


    if ($('.slider-for').length > 0 && $('.slider-nav').length > 0){
        var ap_slider = $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow:'.slider--navigation-prev',
            nextArrow:'.slider--navigation-next',
            // fade: true,
            asNavFor: '.slider-nav',
            focusOnSelect: true,
            adaptiveHeight: true
        });
        var ap_slider_nav= $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            arrows: false,
            infinite: true,
            centerMode: true,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
    }

    if ($('.modal-book--slider').length > 0){
        $('.modal-book--slider').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            infinite:false,
            prevArrow:'.modal-book--slider-prev',
            nextArrow:'.modal-book--slider-next',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
    }

    $('#book').on('show.bs.modal', function (event) {

        $('.modal-book--slider').on('init', function(event, slick){});
        $('.modal-book--slider').slick('slickGoTo','0');

    });


    // $('#book_thx').on('shown.bs.modal', function (e) {
    //     $('#book').modal('hide');
    // })

    $('.modal-apartments--photo *').css('opacity','0');

    $('#apartment').on('show.bs.modal', function (event) {

        $('.slider-for').on('init', function(event, slick){
            var slide = 1;
            $('.slider--navigation-count').html( slide  + '/' + slick.slideCount);
            $('.slider-for').slick('slickGoTo','1');

        }).on('beforeChange', function(event, slick, currentSlide){
            var slide = currentSlide + 1;
            $('.slider--navigation-count').html( slide  + '/' + slick.slideCount);
        });
        $('.slider-nav').on('init', function(event, slick){
            var slide = 1;
            $('.slider-nav').slick('slickGoTo','1');
        });

        $('.slider-for').slick('slickGoTo','1');
        $('.slider-nav').slick('slickGoTo','1');


        setTimeout(function(){
            $('.modal-apartments--photo *').animate({'opacity':'1'},600);
            // $('.slider-for').slick('slickGoTo','1');
            // $('.slider-for').slick('slickGoTo','1');
            // $('.slider-nav').slick('slickGoTo','1');
        }, 600);
        // $('.slider--navigation-next.slick-arrow').click();

    });

    $('#apartment').on('hide.bs.modal', function (event) {
        $('.modal-apartments--photo *').animate({'opacity':'0'},600);
        // $('.slider--navigation-next.slick-arrow').click();
    });

    $('#exampleModal').on('shown.bs.modal', function (event) {
        console.log('Модалка показалась')
        // events_modal.destroy();
        // events_modal.navigation.update();
        // events_modal.slideNext();
    });












    // dropmodal
    // $('body').on('click', '.bookline-black .book-from', function(e){
    //     e.preventDefault();
    //     datepicker_from.show();
    // })
    // $('body').on('click', '.bookline-black .book-to', function(e){
    //     e.preventDefault();
    //     datepicker_to.show();
    // })

    $('body').on('click', '.bookline .bookpicker-child', function(e){
        e.preventDefault();
        countInput($('.bookline .b-child')); // указывать блок в котором input
    })
    $('body').on('click', '.bookline-modal .bookpicker-child-modal', function(e){
        e.preventDefault();
        countInput($('.bookline-modal .b-child')); // указывать блок в котором input

    })

    $('body').on('click', '.bookline .bookpicker-quest', function(e){
        e.preventDefault();
        countInput($('.bookline .b-quest'));
    })
    $('body').on('click', '.bookline-modal .bookpicker-quest-modal', function(e){
        e.preventDefault();
        countInput($('.bookline-modal .b-quest'));
    })

    $(document).mouseup(function (e)
    {
        var container = $(".count");

        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.remove('.count');
        }
    });


    function countInput(el){
        var count = parseInt(el.find('input').val())
        var inputName = el.find('input').attr('name');

        var render = '<div class="count">\n' +
                '<a data-input="'+inputName+'" class="count--minus">–</a>\n' +
                '<span data-input="'+inputName+'" class="count--pin">' + count + '</span>\n' +
                '<a data-input="'+inputName+'" class="count--plus">+</a>\n' +
            '</div>';

        el.append(render);

        // эффект появления
        setTimeout(function(){
            $(el.find('.count')).addClass('active')
        }, 100);

    }
    function countUpdate(count, input){
        $('input[name="'+input +'"]').val(count);
        $('.count .count--pin[data-input="'+input +'"]').text(count);
    }

    function countPlus(input){
        var val = $('.bookline').find('input[name="'+input+'"]').val()
        var count = parseInt(val);
        count=count+1;
        countUpdate(count,input)
    }
    function countMinus(input){
        var val = $('.bookline').find('input[name="'+input+'"]').val()
        var count = parseInt(val);
        if (count == 0){
            count = 0
        } else{
            count=count-1;
        }
        countUpdate(count,input)
    }

    $('body').on('click', '.count--plus', function(e){
        e.preventDefault();
        var input = $(this).attr('data-input');
        countPlus(input);
    });
    $('body').on('click', '.count--minus', function(e){
        e.preventDefault();
        var input = $(this).attr('data-input');
        countMinus(input)
    });


    $('.event--list').masonry({

        itemSelector: '.event--item',
        // use element for option
        columnWidth: 340,
        gutter: 70,
        percentPosition: true,
        horizontalOrder: true
    })


    console.log( "rfrjt-nj ltqcnbdt" );
});



